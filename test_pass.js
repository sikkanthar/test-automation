describe("Test Suite 1", function(){

    // set timeout for all Test cases 60 seconds
    this.timeout(300000)
      
    before(function(){
        
        // do something before test suite execution
        // no matter if there are failed cases // 
    
    });
 
    after(function(){
 
        // do something after test suite execution is finished
        // no matter if there are failed cases
 
    });
    
    beforeEach(function(){
        
        // do something before test case execution
        // no matter if there are failed cases xx
       
    
    });
 
    afterEach(function(){
 
        // do something after test case execution is finished
        // no matter if there are failed cases

        
 
    });

    const {Builder, By, Key, until} = require('selenium-webdriver');
    var assert = require('assert');

    it("Test-1 - Save Employer", function(done){
        
        // test Code
        // assertions
        
        console.log('Started Test 1');

        (async function example() {
        try
        {
                var txt_name = "Create Employee 7000";
                var txt_country = "Australia 7000";

                let driver = await new Builder().forBrowser('firefox').build();
                console.log('Initiated Firefox Driver');

                await driver.get('https://sikkanthar.outsystemscloud.com/SimpleApp/Employees.aspx');
                console.log('Launched Application URL to Create');
            
                await driver.findElement(By.xpath('//*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtActions_wt36"]')).click();
                console.log('Clicked Create New Employee');
                
                await driver.findElement(By.xpath('//*[@id="wt15_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployee_Name"]')).sendKeys(txt_name);
                console.log('Typed Name as '+txt_name);
                            
                await driver.findElement(By.xpath('//*[@id="wt15_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployee_Country"]')).sendKeys(txt_country);
                console.log('Typed Country as '+txt_country);
                            
                await driver.findElement(By.xpath('//*[@id="wt15_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wt26"]')).click();
                console.log('Clicked Save Button');
                

                console.log('Waiting Started');
                await driver.sleep(3000); 
                console.log('Waiting Completed');
                try
                {
                    var the_result = await driver.findElement(By.xpath('//*[@id="wt38_OutSystemsUIWeb_wt21_block_wt2_RichWidgets_wt8_block_wtSanitizedHtml3"]')).getText();
                    console.log(the_result);
                    assert.equal(the_result,'Employee \''+txt_name+'\' was successfully created.');
                    done(); 
                }
                catch (err)
                {
                    console.log(err);
                }
                finally
                {
                    await driver.quit();
                    console.log('Closed Firefox Driver');
                }
    }
        catch (err)
        {
            console.log(err);
        }
        finally
        {
            console.log('Completed Test 1');
           
        }
    })();

}).timeout(30000);
 
    it("Test-2 - Search Employer", function(done){
 
        // test Code
        // assertions

        console.log('Started Test 2');

        (async function example() {
            try
            {

                var txt_name = "Create Employee 7000";
                
                let driver = await new Builder().forBrowser('firefox').build();
                console.log('Initiated Firefox Driver');

                await driver.get('https://sikkanthar.outsystemscloud.com/SimpleApp/Employees.aspx');
                console.log('Launched Application URL to Search');
            
                await driver.findElement(By.xpath('//*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtSearchInput"]')).sendKeys(txt_name);
                console.log('Typed Name as '+txt_name);
                        
                await driver.findElement(By.xpath('//*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wt29"]')).click();
                console.log('Clicked Search Button');

                console.log('Waiting Started');
                await driver.sleep(3000); 
                console.log('Waiting Completed');               
                
                
                //Row 1
                //*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployeeTable_ctl03_wt13"]
                
                // No of Records
                //*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployeeTable_Wrapper"]/div[2]/div[1]/div
                
                try
                {
                    var the_result = await driver.findElement(By.xpath('//*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployeeTable_ctl03_wt13"]')).getText();
                    console.log(the_result);
                        
                    assert.equal(the_result,txt_name);
                    done();
                }
                
                catch (err)
                {
                    console.log(err);
                }
                finally
                {
                    await driver.quit();
                    console.log('Closed Firefox Driver');
                }

    }
    catch (err)
    {
        console.log(err);
    }
    finally
    {
        console.log('Completed Test 2');
        
    }
})();
        
    }).timeout(30000);
 
    it("Test-3 - Modify Employer", function(done){
 
        // test Code
        // assertions
        console.log('Started Test 3');

        (async function example() {
            try
            {

                var orginal_name = "Create Employee 7000";
                var changed_name = "Create Employee 15000";

                let driver = await new Builder().forBrowser('firefox').build();
                console.log('Initiated Firefox Driver');

                await driver.get('https://sikkanthar.outsystemscloud.com/SimpleApp/Employees.aspx');
                console.log('Launched Application URL to Search');
            
                await driver.findElement(By.xpath('//*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtSearchInput"]')).sendKeys(orginal_name);
                console.log('Typed Name as '+ orginal_name);
                        
                await driver.findElement(By.xpath('//*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wt29"]')).click();
                console.log('Clicked Search Button');

                console.log('Waiting Started');
                await driver.sleep(3000); 
                console.log('Waiting Completed');               
                
                
                //Row 1
                //*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployeeTable_ctl03_wt13"]
                
                // No of Records
                //*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployeeTable_Wrapper"]/div[2]/div[1]/div
                
                try
                {
                    var the_result = await driver.findElement(By.xpath('//*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployeeTable_ctl03_wt13"]')).getText();
                    console.log(the_result);

                    await driver.findElement(By.xpath('//*[@id="wt38_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployeeTable_ctl03_wt13"]')).click();
                    console.log('Clicked the row '+ orginal_name);
                    
                    await driver.findElement(By.xpath('//*[@id="wt15_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployee_Name"]')).clear();
                    console.log('Cleared '+ orginal_name);

                    await driver.findElement(By.xpath('//*[@id="wt15_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wtEmployee_Name"]')).sendKeys(changed_name);
                    console.log('Typed Name as '+ changed_name);

                    await driver.findElement(By.xpath('//*[@id="wt15_OutSystemsUIWeb_wt21_block_wtContent_wtMainContent_wt26"]')).click();
                    console.log('Clicked Save Button');

                    console.log('Waiting Started');
                    await driver.sleep(3000); 
                    console.log('Waiting Completed');
                    try
                    {
                        var the_result = await driver.findElement(By.xpath('//*[@id="wt38_OutSystemsUIWeb_wt21_block_wt2_RichWidgets_wt8_block_wtSanitizedHtml3"]')).getText();
                        console.log(the_result);
                        assert.equal(the_result,'Employee \''+changed_name+'\' was successfully updated.');
                        done(); 
                    }
                    catch (err)
                    {
                        console.log(err);
                    }
                    finally
                    {
                        await driver.quit();
                        console.log('Closed Firefox Driver');   
                    }
                    
                }
                
                catch (err)
                {
                    console.log(err);
                }
                finally
                {
                    
                }

    }
    catch (err)
    {
        console.log(err);
    }
    finally
    {
        console.log('Completed Test 3');
    }
})();
 
    }).timeout(30000);

    //it("Test-4 - SelfServe Search Case", function(done){
 
        // test Code
        // assertions
    //    console.log('Started Test 4');

    //    (async function example() {
    //        try
    //        {

     //           var case_id = "123";
                
     //           let driver = await new Builder().forBrowser('firefox').build();
     //           console.log('Initiated Firefox Driver');

     //           await driver.get('https://aihpl-dev.outsystemsenterprise.com/Aspire_IS_Tests/SearchCase.aspx');
    //            console.log('Launched Application URL to Search');
            
    //            await driver.findElement(By.xpath('//*[@id="LiverpoolTheme_wt21_block_wtMainContent_WebPatterns_wt3_block_wtText_wt15"]')).sendKeys(case_id);
     //           console.log('Type Case ID '+ case_id);
                        
    //            await driver.findElement(By.xpath('//*[@id="LiverpoolTheme_wt21_block_wtMainContent_WebPatterns_wt3_block_wtActions_wt1"]')).click();
     //           console.log('Clicked Submit Button');

    //            console.log('Waiting Started');
     //           await driver.sleep(10000); 
     //           console.log('Waiting Completed');               
                
     //           try
     //           {
     //               var the_result = await driver.findElement(By.xpath('//*[@id="json-rendererLiverpoolTheme_wt21_block_wtMainContent_wt4_WebPatterns_wt2_block_wtContent_JSONPrettyFormat_wt3_block"]/ol/li/ul/li[1]/span')).getText();
     //               console.log(the_result);

     //               assert.equal(the_result,case_id);
     //               done(); 
     //           }
     //           catch (err)
     //           {
     //               console.log(err);
     //           }
     //           finally
     //           {
     //               await driver.quit();
     //               console.log('Closed Firefox Driver');   
     //           }
                
    //}
    //catch (err)
    //{
        //console.log(err);
    //}
    //finally
    //{
    //    console.log('Completed Test 4');
    //}
//})();
 
   // }).timeout(90000);
  
});